#!/usr/bin/env bash

TESTING_DIR=blueprints_ci_tests

ARGS=$@
if [ -z $@ ]
then
	ARGS=${TESTING_DIR}/*.py
else
	for item in $@
	do
		ARGS="${TESTING_DIR}.${item}"
	done
fi

if [ -z "${LOGLEVEL}" ]; then
	export LOGLEVEL=CRITICAL
fi

COVERAGE_CMD=
if [ ! -z ${COVERAGE} ]
then
	COVERAGE_CMD="-m coverage run"
	# See test coverage with `python3 -m coverage html`
fi

python3 ${COVERAGE_CMD} -m unittest -v ${ARGS}
